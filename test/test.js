const os = require('os');
const assert = require('assert');
const utilities = require('../services/utilities')

//==============================================================================

const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

//==============================================================================

describe('QUERY', function() {
  describe('#queryEntryExists()', function() {
    it('should return POST1', async function() {
        const result = await sleep(1000).then(() => {
            return utilities.query('queryEntry', ['TEST', '1'])
                .then((result)=>{
                    return utilities.normalize_response(result);
                });
        });

        assert.equal(
          JSON.stringify(result.data),
          JSON.stringify({"type":"TEST","id":1,"timestamp":1224455,"signature":"abdbsnbdsbndbs","ipfs":"koukouroukou"}));
    });
  });

  describe('#queryEntryDoesNotExist()', function() {
    it('should return empty', async function() {
        const result = await sleep(1000).then(() => {
            return utilities.query('queryEntry', ['TEST', '999'])
                .then((result)=>{
                    return utilities.normalize_response(result);
                });
        });

        assert.equal(
          JSON.stringify(result.data),
          JSON.stringify(""));
    });
  });
});

//==============================================================================

describe('CREATE', function() {
  describe('#createEntryCorrect()', function() {
    it('should create/update an entry', async function() {
        const result = await sleep(500).then(() => {
            const type = 'TEST';
            const id = 1234;
            const timestamp = 123456;
            const signature = '35444e7989862d8f9d902c10982036352e6d90d4756c8d9f58e7ffe2029b1022';
            const ipfs = 'e32b66ab7520d135128a5826fe5065c62af9a6133692064f39bfcc69755199e4';
            return utilities.transaction('createEntry', 
            [type, 
              id.toString(), 
              timestamp.toString(), 
              signature,
              ipfs])
                .then((result)=>{
                    return utilities.normalize_response(result);
                });
        });

        assert.equal(
          JSON.stringify(result.data),
          JSON.stringify(true));
    });
  });

  describe('#createEntryWrong()', function() {
    it('should not create an entry (should throw error)', async function() {
        try
        {
          const result = await sleep(500).then(() => {
              const type = 'TEST';
              const id = 1234;
              const timestamp = 123456;
              const ipfs = 'e32b66ab7520d135128a5826fe5065c62af9a6133692064f39bfcc69755199e4';
              return utilities.transaction('createEntry', 
                [type, 
                  id.toString(), 
                  timestamp.toString(), 
                  ipfs])
                    .then((result)=>{
                        assert.ok(false);
                        return utilities.normalize_response(result);
                    });
          });
        }
        catch(error){
          assert.ok(true);
        }
    });
  });
});

//==============================================================================

describe('HISTORY', function() {
  describe('#historyExists()', function() {
    it('should return the history of POST1', async function() {
        const result = await sleep(1000).then(() => {
            return utilities.query('queryEntryHistory', ['TEST', '1'])
                .then((result)=>{
                    return utilities.normalize_response(result);
                });
        });

        assert.equal(
          JSON.stringify(result.data[0].Value),
          JSON.stringify({"type":"TEST","id":1,"timestamp":1224455,"signature":"abdbsnbdsbndbs","ipfs":"koukouroukou"}));
    });
  });

  describe('#historyDoesNotExist()', function() {
    it('should return empty array', async function() {
        const result = await sleep(1000).then(() => {
            return utilities.query('queryEntryHistory', ['TEST', '999'])
                .then((result)=>{
                    return utilities.normalize_response(result);
                });
        });

        assert.equal(
          JSON.stringify(result.data),
          JSON.stringify([]));
    });
  });
});

//==============================================================================
describe('DELETE', function() {
  describe('#deleteEntryExists()', function() {
    it('should return TEST1234', async function() {
        const result = await sleep(1000).then(() => {
            return utilities.query('deleteEntry', ['TEST', '1234'])
                .then((result)=>{
                    return utilities.normalize_response(result);
                });
        });

        assert.equal(
          JSON.stringify(result.data),
          JSON.stringify(true));
    });
  });

  describe('#deleteEntryDoesNotExist()', function() {
    it('should return empty', async function() {
        const result = await sleep(1000).then(() => {
            return utilities.query('deleteEntry', ['TEST', '999'])
                .then((result)=>{
                    return utilities.normalize_response(result);
                });
        });

        assert.equal(
          JSON.stringify(result.data),
          JSON.stringify(false));
    });
  });
});

//-------------------------------------------------------------------
