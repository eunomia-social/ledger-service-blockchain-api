const os = require('os');
const path = require('path');
const util = require('util');
const log4js = require('log4js');
const dotenv = require('dotenv').config();
const utilities = require('./utilities');

//==============================================================================

let logger = log4js.getLogger();

//==============================================================================

module.exports.createEntry = async (options) => {
  try{
    return utilities.transaction('createEntry', 
          [options.body.type, 
            options.body.id.toString().trim(), 
            options.body.timestamp.toString(), 
            options.body.signature,
            options.body.ipfs])
      .then((result)=>{
        return utilities.normalize_response(result);
      });
  }
  catch(error){
      logger.error(`Error : ${error}`);
      throw new ServerError({
        status: 500, 
        error: error
    });
  }
};

//==============================================================================

module.exports.getEntry = async (options) => {
  try{
    return utilities.query('queryEntry', [options.type, options.id.toString().trim()])
      .then((result)=>{
        return utilities.normalize_response(result);
      });
  }
  catch(error){
      logger.error(`Error : ${error}`);
      throw new ServerError({
        status: 500, 
        error: error
    });
  }
};

//==============================================================================

module.exports.getEntryHistory = async (options) => {
  try{
    return utilities.query('queryEntryHistory', [options.type, options.id.toString().trim()])
      .then((result)=>{
        return utilities.normalize_response(result);
      });
  }
  catch(error){
      logger.error(`Error : ${error}`);
      throw new ServerError({
        status: 500, 
        error: error
    });
  }
};

//==============================================================================

module.exports.deleteEntry = async (options) => {
  try{
    return utilities.transaction('deleteEntry', [options.type, options.id.toString().trim()])
      .then((result)=>{
        return utilities.normalize_response(result);
      });
  }
  catch(error){
      logger.error(`Error : ${error}`);
      throw new ServerError({
        status: 500, 
        error: error
    });
  }
};
