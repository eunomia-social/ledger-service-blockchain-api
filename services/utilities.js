const os = require('os');
const fs = require('fs');
const path = require('path');
const util = require('util');
const log4js = require('log4js');
const dotenv = require('dotenv').config();
const Client = require('fabric-client');

//==============================================================================

const ca_url = process.env.CA_URL;
const ca_org = process.env.CA_ORG;
const peer_url = process.env.FABRIC_URL;
const mspid = process.env.FABRIC_USER_MSDPID;
const username = process.env.FABRIC_USERNAME;
const channel_name = process.env.CHANNEL_NAME;
const chaincode_id = process.env.CHAINCODE_ID;
const store_path = path.join(__dirname, process.env.KEY_STORE_PATH);
const fabric_user_profile_path = process.env.FABRIC_USER_PROFILE_PATH;
const fabric_network_profile_path = process.env.FABRIC_NETWORK_PROFILE_PATH;
const peer_discovery_service= (process.env.PEER_DISCOVERY_SERVICE == 'true');
const peer_discover_as_localhost = (process.env.PEER_DISCOVERY_AS_LOCALHOST =='true');

// Get certification from first-network path
const org1_tls_ca_cert_path = process.env.TLS_CERT_PATH;

//==============================================================================
// Setup logger
//==============================================================================

let logger = log4js.getLogger();
logger.level = 'debug';

//==============================================================================

let peer = null;
let org1_tls_ca_cert = '';
let client = Client.loadFromConfig(fabric_network_profile_path);
let channel = client.newChannel(channel_name);

//==============================================================================

// Report on variables
logger.debug(`ca_url = ${ca_url}`);
logger.debug(`ca_org = ${ca_org}`);
logger.debug(`username = ${username}`);
logger.debug(`peer_url = ${peer_url}`);
logger.debug(`channel_name = ${channel_name}`);

//==============================================================================

async function setup_user(){
    const credential_path = fabric_user_profile_path + '/msp';
    const ca_certificate_path = credential_path + '/cacerts/ca.org1.example.com-cert.pem';
    const sign_certificate_path = credential_path + '/signcerts/User1@org1.example.com-cert.pem';

    const files = fs.readdirSync(credential_path + '/keystore/');
    // get first file in keystore
    const private_key_path = credential_path + '/keystore/' + files[0];

    const certificate = fs.readFileSync(sign_certificate_path, 'utf8').toString();
    const private_key = fs.readFileSync(private_key_path, 'utf8').toString();

    return Client.newDefaultKeyValueStore({ path: store_path})
    .then((state_store) => {
        // assign the store to the fabric client
        client.setStateStore(state_store);
        let crypto_suite = Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        let crypto_store = Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        client.setCryptoSuite(crypto_suite);

        return client.createUser({
            mspid: mspid,
            username: username,
            cryptoContent: { 
                privateKeyPEM: private_key, 
                signedCertPEM: certificate 
            }
        });
    });
}

//==============================================================================

function get_ca_certificate(){
    try{
        logger.debug(`Reading CA certificate from ${org1_tls_ca_cert_path}`);
        org1_tls_ca_cert = fs.readFileSync(org1_tls_ca_cert_path, 'utf8');
        logger.debug(`CA certificate read`);
    }
    catch(error){
        logger.error(`Failed to read certificate ${error}`);
        throw new Error('Failed to read certificate');
    }
}

//==============================================================================

async function setup_channel(){
    logger.debug(`Setting up channel ${channel_name} on peer ${peer_url}`);

    peer = client.newPeer(
        peer_url, 
        {
            pem: org1_tls_ca_cert,
            'ssl-target-name-override': 'peer0.org1.example.com'
        });

    return channel.initialize({ 
        discover: peer_discovery_service, 
        asLocalhost: peer_discover_as_localhost, 
        target: peer 
    });
}

//==============================================================================

function query(function_name, args){
    const request = {
        targets: [peer],
        chaincodeId: chaincode_id,
        fcn: function_name,
        args: args ? args : ['']
    };
    
    logger.debug(`Executing chaincode query ${JSON.stringify(request)}`);

	// send the query proposal to the peer
	return channel.queryByChaincode(request);
}

//==============================================================================

async function transaction(function_name, args){
    // Use service discovery to initialize the channel
    await channel.initialize({ 
        discover: peer_discovery_service, 
        asLocalhost: peer_discover_as_localhost, 
        target: peer 
    });
    
    logger.debug('Used service discovery to initialize the channel');

    // get a transaction id object based on the current user assigned to fabric client
    // Transaction ID objects contain more then just a transaction ID, also includes
    // a nonce value and if built from the client's admin user.
    const tx_id = client.newTransactionID();
    logger.debug(`Created a transaction ID: ${tx_id.getTransactionID()}`);

    const request = {
        args: args,
        txId: tx_id,
        targets: [peer],
        fcn: function_name,
        chainId: channel_name,
        chaincodeId: chaincode_id
    };

    logger.debug(`Executing chaincode transaction ${JSON.stringify(request)}`);

    // notice the request has the peer defined in the 'targets' attribute
    // Send the transaction proposal to the endorsing peers.
    // The peers will run the function requested with the arguments supplied
    // based on the current state of the ledger. If the chaincode successfully
    // runs this simulation it will return a positive result in the endorsement.
    return channel.sendTransactionProposal(request)
        .then((endorsement_results) => {
            // The results will contain a few different items
            // first is the actual endorsements by the peers, these will be the responses
            //    from the peers. In our sammple there will only be one results since
            //    only sent the proposal to one peer.
            // second is the proposal that was sent to the peers to be endorsed. This will
            //    be needed later when the endorsements are sent to the orderer.
            const proposal_responses = endorsement_results[0];
            const proposal = endorsement_results[1];

            // check the results to decide if we should send the endorsment to be orderered
            if (proposal_responses[0] instanceof Error) {
                logger.debug('Failed to send Proposal. Received an error :: ' + proposal_responses[0].toString());
                throw proposal_responses[0];
            } 
            else if (proposal_responses[0].response && proposal_responses[0].response.status === 200) {
                logger.debug(`Successfully sent Proposal and received response: Status - ${proposal_responses[0].response.status}`);
            } 
            else {
                const error_message = util.format('Invoke chaincode proposal:: %j', proposal_responses[i]);
                logger.debug(error_message);
                throw new Error(error_message);
            }

            // The proposal was good, now send to the orderer to have the transaction committed
            const commit_request = {
                proposal: proposal,
                proposalResponses: proposal_responses
            };

            // Get the transaction ID string to be used by the event processing
            const transaction_id_string = tx_id.getTransactionID();

            // create an array to hold on the asynchronous calls to be executed at the same time
            const promises = [];

            // this will send the proposal to the orderer during the execuction of the promise 'all' call.
            logger.debug('Sending endorsed transaction to the orderer');
            const send_promise = channel.sendTransaction(commit_request);

            // we want the send transaction first, so that we know where to check status
            promises.push(send_promise);

            // get an event hub that is associated with our peer
            let event_hub = channel.newChannelEventHub(peer);

            // create the asynchronous work item
            let tx_promise = new Promise((resolve, reject) => {
                // setup a timeout of 30 seconds
                // if the transaction does not get committed within the timeout period,
                // report TIMEOUT as the status. This is an application timeout and is a
                // good idea to not let the listener run forever.
                let handle = setTimeout(() => {
                    event_hub.unregisterTxEvent(transaction_id_string);
                    event_hub.disconnect();
                    resolve({event_status : 'TIMEOUT'});
                }, 30000);

                // this will register a listener with the event hub. THe included callbacks
                // will be called once transaction status is received by the event hub or
                // an error connection arises on the connection.
                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                    // this first callback is for transaction event status
                    // callback has been called, so we can stop the timer defined above
                    clearTimeout(handle);

                    // now let the application know what happened
                    const return_status = {event_status : code, tx_id : transaction_id_string};

                    if (code !== 'VALID') {
                        logger.debug('The transaction was invalid, code = ' + code);
                        resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                    } 
                    else {
                        logger.debug('The transaction has been committed on peer ' + event_hub.getPeerAddr());
                        resolve(return_status);
                    }
                }, (err) => {
                    //this is the callback if something goes wrong with the event registration or processing
                    reject(new Error('There was a problem with the eventhub ::'+err));
                },
                //disconnect when complete
                    {disconnect: true} 
                );

                // now that we have a protective timer running and the listener registered,
                // have the event hub instance connect with the peer's event service
                event_hub.connect();
                logger.debug(`Registered transaction listener with the peer event service for transaction ID: ${transaction_id_string}`);
            });

            // set the event work with the orderer work so they may be run at the same time
            promises.push(tx_promise);

            // now execute both pieces of work and wait for both to complete
            return Promise.all(promises);
        })
        .then((results) => {
            // since we added the orderer work first, that will be the first result on the list of results
            // success from the orderer only means that it has accepted the transaction
            // you must check the event status or the ledger to if the transaction was committed
            if (results[0].status === 'SUCCESS') {
                logger.debug('Successfully sent transaction to the orderer');
            }
            else {
                const message = `Failed to order the transaction. Error code: ${results[0].status}`;
                logger.error(message);
                throw new Error(message);
            }

            if (results[1] instanceof Error) {
                logger.error(message);
                throw new Error(message);
            } 
            else if (results[1].event_status === 'VALID') {
                logger.debug('Successfully committed the change to the ledger by the peer');
                return true;
            } 
            else {
                const message = `Transaction failed to be committed to the ledger due to : ${results[1].event_status}`;
                logger.error(message);
                throw new Error(message);
            }
        });
}

//==============================================================================

function normalize_response(result){
    let response = result.toString();
    logger.debug(`Response : [${response}]:[${response.length}]`);
  
    if (response.length > 0){
      response = JSON.parse(response);
    }
  
    return {
      status: 200,
      data: response
    };
}

//==============================================================================

get_ca_certificate();

setup_user()
.then(()=>{
    return setup_channel();
})
.then(()=>{
    logger.debug('Hyperledger Fabric Client READY !!!');
})
.catch((error) =>{
    logger.error(`Failed to setup ${error}`);
    throw error;
});

//==============================================================================

module.exports = {
    query : query,
    transaction: transaction,
    normalize_response : normalize_response
};

//==============================================================================


