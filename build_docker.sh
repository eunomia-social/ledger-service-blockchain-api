#!/bin/bash

IMAGE_REPO=docker.eunomia.inov.pt/blockchain-api
TAG=$1
IMAGE_TAG=""
if [ -z $TAG ]; then
  IMAGE_TAG="$IMAGE_REPO:latest"
else
  IMAGE_TAG="$IMAGE_REPO:$TAG"
fi;

docker build -t $IMAGE_TAG -f docker/Dockerfile .

# docker push $IMAGE_TAG

