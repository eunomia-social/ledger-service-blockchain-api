#!/bin/bash

#FIRSTNET_ROOT_DIR=$PWD/../eunomiafirstnetwork/first-network/
FIRSTNET_ROOT_DIR=$PWD/../ledger-service-dev-network/first-network/

if [ "$1" == 'clean' ]; then
	rm -rf ./node_modules
#	rm -rf ./package-lock.json
	echo "Cleaning key store..."
	rm ./services/hfc-key-store/*
fi;

#docker run -it -d --rm --name frontend --network net_byfn -v ${FIRSTNET_ROOT_DIR}:/first-network  -v $(pwd):/node-app -p9090:9090 --entrypoint /node-app/docker/docker-entrypoint.sh node
docker run -it -d --rm --name frontend-tuple --network net_byfn -v ${FIRSTNET_ROOT_DIR}:/first-network  -v $(pwd):/node-app -p9091:9091 --workdir /node-app --entrypoint /bin/bash node

docker exec -it  frontend-tuple npm install

docker exec -it frontend-tuple npm start
