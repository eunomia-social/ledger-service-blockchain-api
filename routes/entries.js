const log4js = require('log4js');
const express = require('express');
const entries = require('../services/entries');
const router = new express.Router();

//==============================================================================

let logger = log4js.getLogger();

//==============================================================================

router.post('/', async (req, res, next) => {
  const options = {
    body: req.body
  };

  try {
    const result = await entries.createEntry(options);
    res.status(result.status || 200).json(result.data);
  } catch (err) {
    logger.error(`Error : ${err}`);
    return res.status(500).json({
      status: 500,
      error: 'Server Error'
    });
  }
});

//==============================================================================

router.get('/:type/:id', async (req, res, next) => {
  const options = {
    id: req.params['id'],
    type: req.params['type'],
  };

  try {
    const result = await entries.getEntry(options);
    res.status(result.status || 200).json(result.data);
  } catch (err) {
    logger.error(`Error : ${err}`);
    return res.status(500).json({
      status: 500,
      error: 'Server Error'
    });
  }
});

//==============================================================================

router.get('/:type/:id/history', async (req, res, next) => {
  const options = {
    id: req.params['id'],
    type: req.params['type'],
  };

  try {
    const result = await entries.getEntryHistory(options);
    res.status(result.status || 200).json(result.data);
  } catch (err) {
    logger.error(`Error : ${err}`);
    return res.status(500).json({
      status: 500,
      error: 'Server Error'
    });
  }
});

//==============================================================================

router.delete('/:type/:id', async (req, res, next) => {
  const options = {
    id: req.params['id'],
    type: req.params['type'],
  };

  try {
    const result = await entries.deleteEntry(options);
    res.status(result.status || 200).json(result.data);
  } catch (err) {
    logger.error(`Error : ${err}`);
    return res.status(500).json({
      status: 500,
      error: 'Server Error'
    });
  }
});


module.exports = router;
