# EUNOMIA Ledger Service Blockchain API

REST API service for invoking chaincode on EUNOMIA Blockchain Infractructure

## Requirements
* A Hyperledger Fabric Blockchain network deployed and running and
* A Hyperledger Fabric Blockchain user created.
* A Hyperledger Fabric peer node and ```eunomia-tuple``` chaincode deployed and instatiated on it.
and EITHER
* Docker on Linux machine
OR
* node >v10.15 and npm
* Patience for lots of configuration changes

## Developer's Setup
1. Make scripts executables
```bash
chmod +x ./start_frontend.sh
chmod +x ./stop_frontend.sh
```
2. Launch development container
``` bash
./start_frontend.sh
```
3. To stop development container and clear keys run 
```bash
./stop_frontend.sh
```
## Testing
Run the commands in the development container
```bash
docker exec -it eunomia-frontend /bin/bash
```
To run tests
```
npm test
```

## Running
Run the commands in the development container
```bash
docker exec -it eunomia-frontend /bin/bash
```
To run the system type:
```
npm start
```


# API calls

## Entries
```
GET  /entries/{type}/{id}         Retrieves a specific entry by type and id
POST /entries/                    Create / Update a specific entry
GET  /entries/{type}/{id}/history Retrieves the history of a specific entry by type and id
DELETE  /entries/{type}/{id}         Deletes a specific entry by type and id
```

For more information check `OpenAPI` entry


# Contributors
* Antonios Inglezakis (inglezakis.a@unic.ac.cy)


# Funding
This project is funded by the European Union. 
------------------------------
Project title: |  EUNOMIA
Project Number: |  825171
Call: |  H2020-ICT-2018-28
------------------------------
